from debian:bullseye as builder


RUN apt-get update && apt-get install -y g++ clang wget sed bzip2

WORKDIR "/usr/src"

ARG BOOST_VERSION 

RUN VERSION_=$(echo "${BOOST_VERSION}" | sed 's/\./_/g') && \
	 FILENAME=boost_${VERSION_}.tar.bz2 && \
	 DIRNAME=boost_${VERSION_} && \
	 wget https://boostorg.jfrog.io/artifactory/main/release/${BOOST_VERSION}/source/${FILENAME} && \
	 tar -xvf ${FILENAME} && \
	 cd ${DIRNAME} && \
	 ./bootstrap.sh && \
	 ./b2 && \
	 ./b2 install && \
	 cd .. && rm -rf ${DIRNAME} && rm -rf ${FILENAME}

CMD ["/bin/bash"]
