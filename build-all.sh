#!/bin/bash

VERSIONS="1.69.0 1.70.0 1.71.0 1.72.0 1.73.0 1.74.0 1.75.0 1.76.0 1.77.0 1.78.0 1.79.0 1.80.0"

for VERSION in $VERSIONS; do
	./build.sh "$VERSION"
done
