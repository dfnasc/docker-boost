#!/bin/bash

if [ $# -ne 1 ]; then
	exit 1
fi

BOOST_VERSION=$1

IMAGE=dfnasc/boost:${BOOST_VERSION}

docker build --tag ${IMAGE} --build-arg BOOST_VERSION=${BOOST_VERSION} .
